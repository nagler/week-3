import numpy as np
import pandas as pd

from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import tensorflow as tf
from sklearn import preprocessing
import sys

FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_integer('type', 0, """Linear Regression/ Logistic Regression""")
tf.app.flags.DEFINE_string('train_dir', '/tf_model',
                           """Directory where to write event logs """
                           """and checkpoint.""")
tf.app.flags.DEFINE_string('data_dir', '/data',
                           """Directory to data""")
tf.app.flags.DEFINE_integer('epochs', 100,
                            """Number of batches to run.""")
tf.app.flags.DEFINE_integer('log_frequency', 10,
                            """How often to log results to the console.""")
tf.app.flags.DEFINE_integer('batch_size', 256,
                            """Size of batch""")


def run_regression():
    path = FLAGS.data_dir
    df = pd.read_csv(path)
    df['bias'] = 1
    df['MinTemp'] = preprocessing.scale(df['MinTemp'])
    df = df[['MinTemp', 'bias', 'MaxTemp']]
    features, labels = (df[['MinTemp', 'bias']].values,
                        df['MaxTemp'].values.reshape(-1, 1))
    # dataset = tf.data.Dataset.from_tensor_slices((features, labels)).repeat().batch(FLAGS.batch_size)
    # iter = dataset.make_one_shot_iterator()
    #ds = df_to_dataset(df, shuffle=False, batch_size=FLAGS.batch_size)
    #iter = ds.make_one_shot_iterator
    N = len(df)
    X = tf.placeholder(dtype=tf.float32, name='X', shape=[None, 2])
    Y = tf.placeholder(dtype=tf.float32, name='Y', shape=[None, 1])
    W = tf.Variable(np.random.rand(2, 1).astype(np.float32), name='weights')
    Z = tf.matmul(X, W)
    error = Y - Z
    costw = tf.reduce_sum(tf.pow(error, 2)) / (2 * N)

    optimizer = tf.train.GradientDescentOptimizer(learning_rate=1e-1)
    saver = tf.train.Saver()
    writer = tf.summary.FileWriter('./graphs', tf.get_default_graph())

    train_op = optimizer.minimize(costw)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        for epoch in range(FLAGS.epochs):
            # Train step
            num_iter = N/FLAGS.batch_size
            # x,y = iter.get_next()
            for i in range(num_iter):
                # print i
                x, y = features[i * FLAGS.batch_size: i * FLAGS.batch_size + FLAGS.batch_size], labels[i * FLAGS.batch_size: i * FLAGS.batch_size + FLAGS.batch_size]
                # x, y = iter.get_next()
                # x, y = x.eval(), y.eval()
                # print x, y
                # print x, y
                sess.run(train_op, feed_dict={X: x, Y: y})

            # Logs
            if (epoch + 1) % FLAGS.log_frequency == 0:
                # Save intermediate weights
                saver.save(sess, './model/', global_step=epoch)
                x = features.astype(np.float32)
                y = labels.astype(np.float32)
                c = sess.run(costw, feed_dict={X: x, Y: y})  ### usualy we use here EVALUATION data
                print("checkpoint saved. Epoch:", '%04d' % (epoch + 1), "cost=", "{:.9f}".format(c),
                      "W=", sess.run(W))

        print("Optimization Finished!")
        x = features.astype(np.float32)
        y = labels.astype(np.float32)
        training_cost = sess.run(costw, feed_dict={X: x, Y: y})  ### usualy we use here EVALUATION data
        print("Evaluation cost=", training_cost, "W=", sess.run(W), '\n')
        saver.save(sess, './model/', global_step=epoch)
        weights = sess.run(W)


    writer.close()
    fig = plt.figure()
    plt.plot(features, labels, 'ro')
    plt.plot(features, np.matmul(features, weights), color='b')
    plt.show()



def run_clasifier():
    learning_rate = 0.001
    num_epochs = FLAGS.epochs
    batch_size = FLAGS.batch_size
    display_step = FLAGS.log_frequency

    # Data
    path = FLAGS.data_dir
    df = pd.read_csv(path)
    df['bias'] = 1
    df = df[['variance', 'skewness', 'curtosis', 'entropy', 'bias', 'authenticity']]
    features, labels = (df[['variance', 'skewness', 'curtosis', 'entropy', 'bias']].values,
                        df['authenticity'].as_matrix().reshape(-1, 1))
    X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=0.2, random_state=6)

    # Network Parameters
    num_input = X_train.shape[1]
    num_classes = y_train.shape[1]
    num_steps = int(np.ceil(X_train.shape[0]/float(batch_size)))

    # tf Graph input
    X = tf.placeholder("float", [None, num_input])
    Y = tf.placeholder("float", [None, num_classes])
    TEST_FLAG = tf.placeholder("string")

    train_dataset = tf.data.Dataset.from_tensor_slices((X, Y)).batch(batch_size).repeat()
    test_dataset = tf.data.Dataset.from_tensor_slices((X, Y)).batch(y_test.shape[0])
    # create a iterator of the correct shape and type
    iterator_test = tf.data.Iterator.from_structure(train_dataset.output_types,
                                                    train_dataset.output_shapes)
    iterator_train = tf.data.Iterator.from_structure(train_dataset.output_types,
                                                     train_dataset.output_shapes)
    # create the initialisation operations
    train_data_init_op = iterator_train.make_initializer(train_dataset)
    test_data_init_op = iterator_test.make_initializer(test_dataset)

    # Init weights
    w_init = tf.contrib.layers.xavier_initializer()
    W = tf.Variable(w_init([num_input, 1]), name='weights')

    def predict(x):
        Z = tf.math.sigmoid(tf.matmul(x, W))
        return Z

    # if TEST_FLAG == "False":
    #     x, y = iterator_train.get_next()
    # else:
    #     x, y = iterator_test.get_next()

    x, y = iterator_test.get_next() if TEST_FLAG == "True" else iterator_train.get_next()
    y_pred = predict(x)

    # define loss
    loss_op = -tf.reduce_mean(((y * tf.log(y_pred)) + ((1 - y) * tf.log(1 - y_pred))))
    loss_summary = tf.summary.scalar('loss', loss_op)

    # Choose optimizer
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)

    # Define the model training's goal
    train_op = optimizer.minimize(loss_op)

    # Choose evaluation metrics

    correct_pred = tf.equal(tf.cast(y_pred > 0.5, tf.int64), tf.cast(y, tf.int64))

    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
    accuracy_summary = tf.summary.scalar('accuracy', accuracy)

    # Initialize the variables (i.e. assign their default value)
    init = tf.global_variables_initializer()

    # Initialize the Writers
    train_writer = tf.summary.FileWriter('./train_log')
    test_writer = tf.summary.FileWriter('./test_log')

    # Start training
    with tf.Session() as sess:
        # Run the initializers on train data
        print('Initializing Training...')
        sess.run(init)
        sess.run(train_data_init_op, feed_dict={X: X_train, Y: y_train})
        sess.run(test_data_init_op, feed_dict={X: X_test, Y: y_test})
        for epoch in range(num_epochs):
            for step in range(1, num_steps + 1):
                # Run optimization op (backprop)

                _, train_loss_val, train_acc_val, train_loss_summ, train_acc_summ = sess.run(
                    [train_op, loss_op, accuracy, loss_summary, accuracy_summary], feed_dict={TEST_FLAG: "False"})
                if step % 5 == 0 or step == 1:
                    # Display training loss and accuracy
                    print("Epoch: " + str(epoch+1) + " Step " + str(step) + ", Minibatch Loss = " + \
                          "{:.4f}".format(train_loss_val) + ", Training Accuracy = " + \
                          "{:.3f}".format(train_acc_val))

            if (epoch + 1) % FLAGS.log_frequency == 0:
                test_loss_val, test_acc_val, test_loss_summ, test_acc_summ = sess.run(
                    [loss_op, accuracy, loss_summary, accuracy_summary], feed_dict={TEST_FLAG: "True"})
                print("==================")
                print("Epoch " + str(epoch+1) + " Validation Loss: " + str(test_loss_val) + " Accuracy: " + str(test_acc_val))
                print("==================")
                train_writer.add_summary(train_loss_summ, epoch)
                train_writer.add_summary(train_acc_summ, epoch)
                test_writer.add_summary(test_loss_summ, epoch)
                test_writer.add_summary(test_acc_summ, epoch)

    print("Optimization Finished!")


def main(argv=None):  # pylint: disable=unused-argument
    # if tf.gfile.Exists(FLAGS.train_dir):
    #   tf.gfile.DeleteRecursively(FLAGS.train_dir)
    # tf.gfile.MakeDirs(FLAGS.train_dir)
    if FLAGS.type:
        run_clasifier()
    else:
        run_regression()

if __name__ == '__main__':
    print sys.path
    tf.app.run()

# # # #  DEPRECATED XD # # # #
# def run_clasifier():
#
#     path = FLAGS.data_dir
#     df = pd.read_csv(path)
#     df['bias'] = 1
#     df = df[['variance', 'skewness', 'curtosis', 'entropy', 'bias', 'authenticity']]
#     features, labels = (df[['variance', 'skewness', 'curtosis', 'entropy', 'bias']].values,
#                         df['authenticity'].as_matrix().reshape(-1, 1))
#     feat_size = features.shape[1]
#     # dataset = tf.data.Dataset.from_tensor_slices((features, labels)).repeat().batch(FLAGS.batch_size)
#     # iter = dataset.make_one_shot_iterator()
#     # ds = df_to_dataset(df, shuffle=False, batch_size=FLAGS.batch_size)
#     # iter = ds.make_one_shot_iterator
#     N = len(df)
#     X = tf.placeholder(dtype=tf.float32, name='X', shape=[None, feat_size])
#     Y = tf.placeholder(dtype=tf.float32, name='Y', shape=[None, 1])
#     W = tf.Variable(np.random.rand(feat_size, 1).astype(np.float32), name='weights')
#     Z = tf.math.sigmoid(tf.matmul(X, W))
#     #error = tf.losses.sigmoid_cross_entropy(Y, Z)
#     error = -tf.reduce_mean(((Y*tf.log(Z))+((1-Y)*tf.log(1-Z))))
#     optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.05)
#     saver = tf.train.Saver()
#     writer = tf.summary.FileWriter('./graphs', tf.get_default_graph())
#
#     train_op = optimizer.minimize(error)
#
#     with tf.Session() as sess:
#         sess.run(tf.global_variables_initializer())
#         for epoch in range(FLAGS.epochs):
#             # Train step
#             num_iter = N / FLAGS.batch_size
#
#             # x,y = iter.get_next()
#             for i in range(num_iter):
#                 # print i
#                 x, y = features[i * FLAGS.batch_size: i * FLAGS.batch_size + FLAGS.batch_size], labels[i * FLAGS.batch_size: i * FLAGS.batch_size + FLAGS.batch_size]
#                 # x, y = iter.get_next()
#                 # x, y = x.eval(), y.eval()
#                 # print x, y
#                 # print x, y
#                 sess.run(train_op, feed_dict={X: x, Y: y})
#
#             # Logs
#             if (epoch + 1) % FLAGS.log_frequency == 0:
#                 # Save intermediate weights
#                 saver.save(sess, './model/', global_step=epoch)
#                 x = features.astype(np.float32)
#                 y = labels.astype(np.float32)
#                 c = sess.run(error, feed_dict={X: x, Y: y})  ### usualy we use here EVALUATION data
#                 print("checkpoint saved. Epoch:", '%04d' % (epoch + 1), "cost=", "{:.9f}".format(c),
#                       "W=", sess.run(W))
#
#         print("Optimization Finished!")
#         x = features.astype(np.float32)
#         y = labels.astype(np.float32)
#         training_cost = sess.run(error, feed_dict={X: x, Y: y})  ### usualy we use here EVALUATION data
#         print("Evaluation cost=", training_cost, "W=", sess.run(W), '\n')
#         saver.save(sess, './model/', global_step=epoch)
#         weights = sess.run(W)
#         prob = sess.run(Z, feed_dict={X: x, Y: y})
#         correct_pred = tf.equal(tf.round(prob), Y)
#         accuracy = sess.run(tf.reduce_mean(tf.cast(correct_pred, tf.float32)), feed_dict={X: x, Y: y})
#
#         print "Accuracy: ",accuracy
