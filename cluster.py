import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns; sns.set()
import pandas as pd
from numpy.linalg import norm
from scipy.spatial.distance import cdist
from sklearn import metrics
from sklearn.mixture import GaussianMixture
from sklearn.cluster import DBSCAN


def initialize_centroids(points, k):
    """returns k centroids from the initial points"""
    centroids = np.copy(points)
    np.random.shuffle(centroids)
    return centroids[:k]


def closest_centroid(data, c):
    c_ex = c[: , np.newaxis, :]
    """returns an array containing the index to the nearest centroid for each point"""
    return np.argmin(np.sqrt(((data - c_ex)**2).sum(axis=2)), axis=0)





def move_centroids(data, closest, centroids):
    """returns the new centroids assigned from the points closest to them"""
    calc = np.array([data[closest == k].mean(axis=0) for k in range(centroids.shape[0])])
    return calc


def KMeans(data, k):
    i=0
    c=initialize_centroids(data, k)
    close = closest_centroid(data, c)
    new_centroid = move_centroids(data, close, c)
    while norm(new_centroid-c) > 1e-6:
        i+=1
        c=new_centroid
        close = closest_centroid(data, c)
        new_centroid=move_centroids(data, close, c)
    c=new_centroid
    return c


def fit(centroids, data):
    return closest_centroid(data, centroids)


def main():
    points = pd.read_csv('bacteria.txt')
    sns.scatterplot(points.x, points.y)
    points=points.values
    # ax = plt.gca()
    #I can observe 7 clusters
    k = 7
    KM_cluster = KMeans(points, k)
    km_labels = fit(KM_cluster, points)
    plt.figure(1)
    plt.scatter(points[:, 0], points[:, 1], c=km_labels, cmap=plt.get_cmap('coolwarm'))
    plt.title("Scatter Plot of Bacteria with Kmeans K=%d" % k)
    plt.show(block=False)
    # Elbow Curve:
    distortions = []
    K = range(1, 10)
    for ik in range(1, 10):
        c = KMeans(points, ik)
        distortions.append(sum(np.min(cdist(points, c, 'euclidean'), axis=1)) / points.shape[0])
    plt.figure(2)
    plt.plot(K, distortions, 'bx-')
    plt.xlabel('k')
    plt.ylabel('Distortion')
    plt.title('The Elbow Method showing the optimal k')
    plt.show(block=False)
    k=5
    EM_Model = GaussianMixture(n_components=k)
    EM_Model.fit(points)
    em_labels = EM_Model.predict(points)
    plt.figure(3)
    plt.scatter(points[:, 0], points[:, 1], c=em_labels, cmap=plt.get_cmap('coolwarm'))
    plt.title("Scatter Plot of Bacteria with EM K=%d" % k)
    plt.show(block=False)
    epsilon, n = 2.10, 15
    db_Model = DBSCAN(eps=epsilon, min_samples=n)
    db_Model.fit(points)
    db_labels = db_Model.labels_
    plt.figure(4)
    plt.scatter(points[:, 0], points[:, 1], c=db_labels, cmap=plt.get_cmap('gist_earth'))
    plt.title("Scatter Plot of Bacteria with DBSCAN eps %f and n %f" % (epsilon, n))
    plt.show(block=True)


if __name__ == "__main__":
    main()

